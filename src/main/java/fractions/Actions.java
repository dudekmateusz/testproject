//@author kamil.trzcinski@gmail.com
package fractions;

public class Actions {

    public Actions() {
    }

    ;

    public VulgarFraction reduceFraction(VulgarFraction vf) {
        int gcd = gcd(vf.getNumerator(), vf.getDenominator());
        vf.setNumerator(vf.getNumerator() / gcd);
        vf.setDenominator(vf.getDenominator() / gcd);
        return vf;
    }

    public VulgarFraction invertFraction(VulgarFraction vf) {
        int tmp = vf.getNumerator();
        vf.setNumerator(vf.getDenominator());
        vf.setDenominator(tmp);
        return vf;
    }

    public DecimalFraction vulgarToDecimal(VulgarFraction vf) {
        DecimalFraction df = new DecimalFraction((double) vf.getNumerator() / (double) vf.getDenominator());
        return df;
    }

    public VulgarFraction decimalToVulgar(DecimalFraction df) {
        VulgarFraction vf = new VulgarFraction();
        String tmp = String.valueOf(df);
        String[] tmpS = tmp.split("\\.");
        tmp = tmpS[1];
        if (df.getDecimalFraction() < 0) {
            vf.setNumerator(-Integer.parseInt(tmp));
        } else {
            vf.setNumerator(Integer.parseInt(tmp));
        }

        char[] tmpC = tmp.toCharArray();
        tmp = "1";
        for (int i = 0; i < tmpC.length; i++) {
            tmp += "0";
        }
        if (Integer.parseInt(tmpS[0]) != 0) {
            vf.setNumerator(vf.getNumerator() + Integer.parseInt(tmp) * Integer.parseInt(tmpS[0]));
        }

        vf.setDenominator(Integer.parseInt(tmp));

        int gcd = gcd(vf.getNumerator(), vf.getDenominator());
        vf.setNumerator(vf.getNumerator() / gcd);
        vf.setDenominator(vf.getDenominator() / gcd);
        return vf;
    }

    public VulgarFraction addVulgarFraction(VulgarFraction vf1, VulgarFraction vf2) {
        VulgarFraction vf = new VulgarFraction();
        int lcm = lcm(vf1.getDenominator(), vf2.getDenominator());
        vf.setDenominator(lcm);
        if (vf1.getDenominator() != lcm && vf2.getDenominator() != lcm) {
            cd(vf1, lcm);
            cd(vf2, lcm);
        }
        if (vf1.getDenominator() != lcm) cd(vf1, vf2.getDenominator() / vf1.getDenominator());
        if (vf2.getDenominator() != lcm) cd(vf1, vf1.getDenominator() / vf2.getDenominator());
        vf.setNumerator(vf1.getNumerator() + vf2.getNumerator());

        return reduceFraction(vf);
    }

    public VulgarFraction subVulgarFraction(VulgarFraction vf1, VulgarFraction vf2) {
        VulgarFraction vf = new VulgarFraction();
        int lcm = lcm(vf1.getDenominator(), vf2.getDenominator());
        vf.setDenominator(lcm);
        if (vf1.getDenominator() != lcm && vf2.getDenominator() != lcm) {
            cd(vf1, lcm);
            cd(vf2, lcm);
        }
        if (vf1.getDenominator() != lcm) cd(vf1, vf2.getDenominator() / vf1.getDenominator());
        if (vf2.getDenominator() != lcm) cd(vf1, vf1.getDenominator() / vf2.getDenominator());
        vf.setNumerator(vf1.getNumerator() - vf2.getNumerator());

        return reduceFraction(vf);
    }

    public VulgarFraction mulVulgarFraction(VulgarFraction vf1, VulgarFraction vf2) {
        VulgarFraction vf = new VulgarFraction(vf1.getNumerator() * vf2.getDenominator(),
                vf1.getDenominator() * vf2.getNumerator());
        return reduceFraction(vf);
    }

    public VulgarFraction divVulgarFraction(VulgarFraction vf1, VulgarFraction vf2) {
        return mulVulgarFraction(vf1, invertFraction(vf2));
    }

    //    greatest common divisor
    private int gcd(int a, int b) {
        if (b == 0)
            return a;
        return gcd(b, (a % b));
    }

    //    least common multiple
    private int lcm(int a, int b) {
        return a / gcd(a, b) * b;
    }

    //    common denominator
    private VulgarFraction cd(VulgarFraction vf, int mul) {
        vf.setNumerator(vf.getNumerator() * mul);
        vf.setDenominator(vf.getDenominator() * mul);
        return vf;
    }
}
