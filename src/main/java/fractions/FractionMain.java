package fractions;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * main class opening program
 * @author Mateusz Dudek
 *
 */

public class FractionMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        AnchorPane anchorPane = FXMLLoader.load(getClass().getResource("/testView.fxml"));
        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("HelloFx!");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
