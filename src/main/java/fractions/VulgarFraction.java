//@author kamil.trzcinski@gmail.com
package fractions;

public class VulgarFraction {
    private int numerator;
    private int denominator;

    public VulgarFraction(){};
    public VulgarFraction(int numerator, int denominator){
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        return numerator+"\n"+denominator;
    }
}
